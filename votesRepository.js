var votesCollection;

const logger = require('./logger.js');

function VotesRepository(coll) {
  votesCollection = coll;
}

VotesRepository.prototype.find = async function(postId, userId) {
  let q = { userId: userId, postId: postId };
  let res = await votesCollection.findOne(q);
  return res;
}

VotesRepository.prototype.update = async function(postId, userId, val) {
  let q = { userId: userId, postId: postId };
  let res = await votesCollection
    .findOneAndUpdate(q, { $set: { val: val } });
  return res;
}

VotesRepository.prototype.add = async function(vote) {
  let res = await votesCollection.insert(vote);
  return res;
}

VotesRepository.prototype.delete = async function(postId, userId) {
  let q = { userId: userId, postId: postId };
  let res = await votesCollection.findOneAndDelete(q);
  return res;
}

module.exports = VotesRepository;