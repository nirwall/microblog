let decorated;
let nowFn;
let lastUpdate;
let cachedEntries;

const logger = require('./logger.js');

function CachingTopCalculator(calc, now) {
  decorated = calc;
  nowFn = now;
  let offset = (24*60*60*1000) * 7;
  let f = nowFn();
  f.setTime(f.getTime() - offset);
  lastUpdate = + f;
}

CachingTopCalculator.prototype.get = async function() {
  let now = nowFn();
  if (now - lastUpdate > 29 * 1000) {
      cachedEntries = decorated.get();
      lastUpdate = now;
  }
  return cachedEntries;
}

module.exports = CachingTopCalculator;