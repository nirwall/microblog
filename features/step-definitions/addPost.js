const should = require('should');
const rp = require('request-promise-native');
const config = require('../../config.json');
const common = require('../common.js');

const {
  Given,
  When,
  Then,
  SetWorldConstructor,
  defineSupportCode
} = require('cucumber');

Given('I am using a valid add post request', async function() {
  this.userId = 'user1';
  this.addRequest = common.addRequest(this.userId);
})

Given('I am using a add post request with no user id', async function() {
  this.addRequest = common.addRequest('dummy');
  delete this.addRequest.headers.authorization;
})

When('I add the post', async function() {
  try {
    this.response = await rp(this.addRequest);
  } catch(e){
    this.response = e;
  }
});

Then('the response should have an id', async function() {
  let body = this.response.body;
  body.should.have.property('id');
});

Then('the response should have status code {int}', function (statusCode) {
  this.response.should.have.property('statusCode');
  this.response.statusCode.should.equal(statusCode);
});

Then('the response should be a {int} error', function (statusCode) {
  this.response.should.have.property('statusCode');
  this.response.statusCode.should.equal(statusCode);
});