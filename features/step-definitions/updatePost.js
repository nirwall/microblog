const should = require('should');
const rp = require('request-promise-native');
const config = require('../../config.json');
const common = require('../common.js');

const {
  Given,
  When,
  Then,
  SetWorldConstructor,
  defineSupportCode
} = require('cucumber');

Given('I am using an existing post id', async function() {
  this.userId = 'user1';
  this.addRequest = common.addRequest(this.userId);

  let response = await rp(this.addRequest);
  this.postId = response.body.id;
})

Given('I am using a non existent post id', async function () {
  this.postId = 'non-existant';
  this.userId = 'user1';
  this.updateRequest = common.updateRequest(this.postId, this.userId);
});

Given('I am using a valid update post request', async function() {
  this.updateRequest = common.updateRequest(this.postId, this.userId);
})

Given(`I am using a different user id than the post's`, function () {
  this.updateRequest = common.updateRequest(this.postId, 'anotherUser');
});

When('I update the post', async function() {
  try {
    this.response = await rp(this.updateRequest);
  } catch(e){
    this.response = e;
  }
});

Then('the response should have the updated content', function () {
  let body = this.response.body;
  body.content.should.equal(this.updateRequest.body.content);
});