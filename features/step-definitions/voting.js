const should = require('should');
const rp = require('request-promise-native');
const config = require('../../config.json');
const common = require('../common.js');

const {
  Given,
  When,
  Then,
  SetWorldConstructor,
  defineSupportCode
} = require('cucumber');

When('I upvote the post', async function () {
  let request = common.upvoteRequest(this.postId, this.userId);
  try {
    this.response = await rp(request);
  } catch (e) {
    this.response = e;
  }
});

When('I upvote the post with user {string}', async function (userId) {
  let request = common.upvoteRequest(this.postId, userId);
  try {
    this.response = await rp(request);
  } catch (e) {
    this.response = e;
  }
});


When('I downvote the post', async function () {
  let request = common.downvoteRequest(this.postId, this.userId);
  try {
    this.response = await rp(request);
  } catch (e) {
    this.response = e;
  }
});

When('I downvote the post with user {string}', async function (userId) {
  let request = common.downvoteRequest(this.postId, userId);
  try {
    this.response = await rp(request);
  } catch (e) {
    this.response = e;
  }
});

When('I remove my vote', async function () {
  let request = common.unvoteRequest(this.postId, this.userId);
  try {
    this.response = await rp(request);
  } catch (e) {
    this.response = e;
  }
});

Then(`the post's upvote count should be {int}`, async function (upvoteCount) {
  let getRequest = common.getRequest(this.postId, this.userId)
  let post = await rp(getRequest);

  post.body.should.have.property('up');
  post.body.up.should.equal(upvoteCount);
});

Then(`the post's downvote count should be {int}`, async function (downvoteCount) {
  let getRequest = common.getRequest(this.postId, this.userId)
  let post = await rp(getRequest);

  post.body.should.have.property('down');
  post.body.down.should.equal(downvoteCount);
});