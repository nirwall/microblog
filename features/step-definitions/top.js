const should = require('should');
const rp = require('request-promise-native');
const config = require('../../config.json');
const common = require('../common.js');

const {
  Given,
  When,
  Then,
  SetWorldConstructor,
  defineSupportCode
} = require('cucumber');

Given('a bunch of votes are made', {timeout: 60 * 1000}, async function() {
  let hotPosts = [];

  for (let i = 0; i < 5; ++i) {
    let upvotes = getRandomInt(10, 20);
    let downvotes = getRandomInt(0, 3);
    let id = await addPostAndVotes(upvotes, downvotes);
    hotPosts.push(id);
  }

  for (let i = 0; i < 5; ++i) {
    let upvotes = getRandomInt(0, 3);
    let downvotes = getRandomInt(10, 20);
    await addPostAndVotes(upvotes, downvotes);
  }

  this.expected = hotPosts;
})

Given('the top list gets refreshed', {timeout: 120 * 1000}, async function() {
  let ts = 61000;
  await timeout(ts);
})

When('I get the top list', async function () {
  let request = common.topRequest();
  this.topResponse = await rp(request);
})

Then('the list should include the hot posts', function () {
  let actual = this.topResponse.body;
  for (let id of this.expected) {
    let match = actual.some((x) => x.id === id);
    match.should.be.true();
  }
})

async function addPostAndVotes(up, down) {
  let userId = common.getUserId();
  this.addRequest = common.addRequest(userId);
  this.response = await rp(this.addRequest);
  let postId = response.body.id;

  let requests = [];
  for (let i = 0; i < up; ++i) {
    let userId = common.getUserId();
    let request = common.upvoteRequest(postId, userId);
    requests.push(rp(request));
  }

  for (let i = 0; i < down; ++i) {
    let userId = common.getUserId();
    let request = common.downvoteRequest(postId, userId);
    requests.push(rp(request));
  }
  await Promise.all(requests);

  return postId;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

const timeout = ms => new Promise(res => setTimeout(res, ms))