﻿Feature: Edit Post

Scenario: Valid request
  Given I am using an existing post id
    And I am using a valid update post request
  When I update the post
  Then the response should have status code 200
    And the response should have the updated content

Scenario: Non existent post id
  Given I am using a non existent post id
  When I update the post
  Then the response should be a 404 error

Scenario: Post of different user
  Given I am using an existing post id
    But I am using a different user id than the post's
  When I update the post
  Then the response should be a 403 error

