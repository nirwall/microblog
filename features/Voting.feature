﻿Feature: Voting

Scenario: Valid Upvote
  Given I am using an existing post id
  When I upvote the post
  Then the response should have status code 200
    And the post's upvote count should be 1
    And the post's downvote count should be 0

Scenario: Upvote non existent post id
  Given I am using a non existent post id
  When I upvote the post
  Then the response should be a 404 error

Scenario: Double Upvote
  Given I am using an existing post id
  When I upvote the post
    And I upvote the post
  Then the response should have status code 400
    And the post's upvote count should be 1
    And the post's downvote count should be 0

Scenario: Valid Downvote
  Given I am using an existing post id
  When I downvote the post
  Then the response should have status code 200
    And the post's upvote count should be 0
    And the post's downvote count should be 1

Scenario: Downvote non existent post id
  Given I am using a non existent post id
  When I downvote the post
  Then the response should be a 404 error

Scenario: Double Downvote
  Given I am using an existing post id
  When I downvote the post
    And I downvote the post
  Then the response should have status code 400
    And the post's upvote count should be 0
    And the post's downvote count should be 1

Scenario: Unvote
  Given I am using an existing post id
  When I remove my vote
  Then the response should have status code 200
    And the post's upvote count should be 0
    And the post's downvote count should be 0

Scenario: Undo Upvote
  Given I am using an existing post id
  When I upvote the post
    And I remove my vote
  Then the post's upvote count should be 0
    And the post's downvote count should be 0

Scenario: Undo Downvote
  Given I am using an existing post id
  When I downvote the post
    And I remove my vote
  Then the post's upvote count should be 0
    And the post's downvote count should be 0

Scenario: Upvote then downvote
  Given I am using an existing post id
  When I upvote the post
    And I downvote the post
  Then the post's upvote count should be 0
    And the post's downvote count should be 1

Scenario: Downvote then upvote
  Given I am using an existing post id
  When I downvote the post
    And I upvote the post
  Then the post's upvote count should be 1
    And the post's downvote count should be 0

Scenario: Multiple votes
  Given I am using an existing post id
  When I downvote the post with user 'user1'
    And I downvote the post with user 'user2'
    And I downvote the post with user 'user3'
    And I upvote the post with user 'user4'
    And I upvote the post with user 'user5'
    And I upvote the post with user 'user6'
  Then the post's upvote count should be 3
    And the post's downvote count should be 3
