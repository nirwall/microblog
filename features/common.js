'use strict';

const crypto = require('crypto');

function Common() {
  const env = process.env.ENVIRONMENT_NAME || 'prod';
  const baseUrl = process.env.BASE_URL;
  console.log(`env: ${env}`)
  let allConfigs = require('./config.json');
  let config = allConfigs[env];
  this.baseUrl = baseUrl || config.baseUrl;
  console.log(`baseUrl: ${this.baseUrl}`)
}

Common.prototype.generate = function(len) {
  return crypto
    .randomBytes(Math.ceil(len * 3 / 4))
    .toString('base64') // convert to base64 format
    .slice(0, len) // return required number of characters
    .replace(/\+/g, '0') // replace '+' with '0'
    .replace(/\//g, '0'); // replace '/' with '0'
}

Common.prototype.getUserId = function() {
  return this.generate(10);
}

Common.prototype.getPost = function() {
  return {
    content: this.generate(50),
  }
}

Common.prototype.addRequest = function(userId) {
  let post = this.getPost();

  let request = {
    method: 'POST',
    uri: `${this.baseUrl}/posts/`,
    headers: {
      'authorization': userId,
      'content-type': 'application/json'
    },
    body: post,
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

Common.prototype.getRequest = function(postId, userId) {
  let post = this.getPost();

  let request = {
    method: 'GET',
    uri: `${this.baseUrl}/posts/${postId}`,
    headers: {
      'authorization': userId,
      'content-type': 'application/json'
    },
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

Common.prototype.updateRequest = function(postId, userId) {
  let post = this.getPost();

  let request = {
    method: 'PUT',
    uri: `${this.baseUrl}/posts/${postId}`,
    headers: {
      'authorization': userId,
      'content-type': 'application/json'
    },
    body: post,
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

Common.prototype.upvoteRequest = function(postId, userId) {
  let request = {
    method: 'POST',
    uri: `${this.baseUrl}/posts/${postId}/votes/up`,
    headers: {
      'authorization': userId,
      'content-type': 'application/json'
    },
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

Common.prototype.downvoteRequest = function(postId, userId) {
  let request = {
    method: 'POST',
    uri: `${this.baseUrl}/posts/${postId}/votes/down`,
    headers: {
      'authorization': userId,
      'content-type': 'application/json'
    },
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

Common.prototype.unvoteRequest = function(postId, userId) {

  let request = {
    method: 'DELETE',
    uri: `${this.baseUrl}/posts/${postId}/votes/`,
    headers: {
      'authorization': userId,
      'content-type': 'application/json'
    },
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

Common.prototype.topRequest = function() {
  let post = this.getPost();

  let request = {
    method: 'GET',
    uri: `${this.baseUrl}/top`,
    headers: {
      'authorization': 'someUser',
      'content-type': 'application/json'
    },
    json: true,
    resolveWithFullResponse: true
  };

  return request;
}

module.exports = new Common();
