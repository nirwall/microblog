﻿Feature: Add post

Scenario: Valid request
  Given I am using a valid add post request
  When I add the post
  Then the response should have status code 200
    And the response should have an id
    # And the response should have the same timestamp
    #
Scenario: Unauthenticated request
  Given I am using a add post request with no user id
  When I add the post
  Then the response should have status code 401
