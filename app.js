const express = require('express');
const createError = require('http-errors');
const morgan = require('morgan');
const mongo = require('mongodb');
const monk = require('monk');
const db = monk('localhost:27017/test');
const logger = require('./logger.js');
const asyncHandler = require('express-async-handler')

const app = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// set the user id
app.use(function (req, res, next) {
  console.log(JSON.stringify(req.headers));
  let userId = req.headers['authorization'];

  if (!userId) {
    return next({
      "status": 401,
      "message": "Unauthorized"
    });
  }

  req.userId = userId;
  return next();
});

function configureErrorHandling(app) {
  // catch 404 and forward to error handler
  app.use(async function(req, res, next) {
    console.log('Not Found');
    next(createError(404));
  });

  // error handler
  app.use(function(err, req, res, next) {

    logger.error('error:');
    logger.error(err);

    res.status(err.status || 500);
    res.type('text/plain');
    res.send(err.message);
  });
}

module.exports = function(config) {

  // Composition root and configuration
  const postsCollection = db.get('posts');
  const votesCollection = db.get('votes');
  const topCollection = db.get('top');
  const Posts = require('./routes/posts');
  const postsRepo = new (require('./postsRepository.js'))(postsCollection);
  const votesRepo = new (require('./votesRepository.js'))(votesCollection);
  const posts = new Posts(postsRepo);
  const Votes = require('./routes/votes');
  const votes = new Votes(postsRepo, votesRepo);
  const nowFn = () => new Date();
  const top = new (require('./routes/top'))(topCollection, nowFn);

  app.get('/posts/:id', asyncHandler(posts.get));
  app.post('/posts/', asyncHandler(posts.add));
  app.put('/posts/:id', asyncHandler(posts.update));
  app.post('/posts/:postId/votes/up', asyncHandler(votes.upvote));
  app.post('/posts/:postId/votes/down', asyncHandler(votes.downvote));
  app.delete('/posts/:postId/votes', asyncHandler(votes.unvote));
  app.get('/top/', asyncHandler(top.get));

  configureErrorHandling(app);
  return app;
};
