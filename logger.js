const util = require('util');
const winston = require('winston');
const strftime = require('strftime');
const cd = {
  debug: 'gray',
  info: 'white',
  warn: 'yellow',
  error: 'red'
};

winston.addColors(cd);

const logger = new winston.Logger({
  level: 'debug',
  transports: [
    // https://stackoverflow.com/questions/30414021/winston-improper-log-configuration
    new (winston.transports.Console)({
      level: 'info',

      prettyPrint: true,
      colorize: true,
      silent: false,
      //timestamp: true,
      timestamp: () => strftime('%H:%M:%S.%L'),

      formatter: function (options) {
        let s = `${options.timestamp()}|`
          + `${options.level.toUpperCase().padEnd(5, ' ')}|`
          + (options.message || '')
          + (options.meta && Object.keys(options.meta).length
            // ? `\n\t${JSON.stringify(options.meta), true, 2}`
            ? `\n${util.inspect(options.meta)}`
            : '');
        // https://github.com/winstonjs/winston/issues/603
        // https://github.com/winstonjs/winston/issues/559
        return options.level !== 'info'
          ? winston.config.colorize(options.level, s)
          : s;
      }
    }),

    // new (winston.transports.File)({ filename: 'somefile.log' })
  ]
});

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
if (!String.prototype.padStart) {
  String.prototype.padStart = function padStart(targetLength, padString) {
    targetLength = targetLength >> 0; //floor if number or convert non-number to 0;
    padString = String(padString || ' ');
    if (this.length > targetLength) {
      return String(this);
    }
    else {
      targetLength = targetLength - this.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
      }
      return padString.slice(0, targetLength) + String(this);
    }
  };
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padEnd
// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/repeat
if (!String.prototype.padEnd) {
  String.prototype.padEnd = function padEnd(targetLength, padString) {
    targetLength = targetLength >> 0; //floor if number or convert non-number to 0;
    padString = String(padString || ' ');
    if (this.length > targetLength) {
      return String(this);
    }
    else {
      targetLength = targetLength - this.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
      }
      return String(this) + padString.slice(0, targetLength);
    }
  };
}

module.exports = logger;
