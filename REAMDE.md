
## local setup

- Install node v8.8.1
- Install mongodb >= 2.6.12 
- Run mongodb using `mongod --dbpath {some existing directory}`
- `npm install`
- Run the top list updater job using: `node cron2.js`
- Run the server using: `node bin/wwww`

## integration tests

The test scenarions are described in `features/*.feature`.

#### setting up

By default tests will run against `https://microblog-nirwall.c9users.io`. If you want to run against a local setup, set `ENVIRONMENT_NAME` environment variable to `local`

From the project directory run: `npm install`

#### running

```shell
`node ./node_modules/cucumber/bin/cucumber-js -f node_modules/cucumber-pretty
```

## example requests

The requests are available as a postman collection in `microblog.postman_collection.json`.

#### add post

```shell
curl https://microblog-nirwall.c9users.io/posts/ -i -H 'Authorization: 123' -H 'Content-Type: application/json' -d '{"content":"original"}'
```

#### read post

```shell
curl https://microblog-nirwall.c9users.io/posts/{postId} -i -H 'Authorization: 123'
```

#### update a post

```shell
curl -XPUT https://microblog-nirwall.c9users.io/posts/{postId} -i -H 'Authorization: 123' -H 'Content-Type: application/json' -d '{"content":"updated"}'
```

#### upvote a post

```shell
curl -XPOST https://microblog-nirwall.c9users.io/posts/{postId}/votes/up -i -H 'Authorization: 123' -H 'Content-Type: application/json'
```

#### downvote a post

```shell
curl -XPOST https://microblog-nirwall.c9users.io/posts/{postId}/votes/down -i -H 'Authorization: 123' -H 'Content-Type: application/json'
```

#### delete a vote

```shell
curl -XDELETE https://microblog-nirwall.c9users.io/posts/{postId}/votes -i -H 'Authorization: 123'
```

#### view toplist

```shell
curl https://microblog-nirwall.c9users.io/top -i -H 'Authorization: 123'
```
