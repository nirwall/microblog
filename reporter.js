let reporter = require('cucumber-html-reporter');

let options = {
  theme: 'bootstrap',
  jsonFile: 'test/report/cucumber_report.json',
  output: 'test/report/cucumber_report.html',
  reportSuiteAsScenarios: true,
  launchReport: true
};

reporter.generate(options);
