let postsRepository;

const logger = require('../logger.js');
const moment = require('moment');

function Posts(repo) {
  postsRepository = repo;
}

Posts.prototype.update = async function(req, res, next) {
  let id = req.params.id;
  let post = await postsRepository.find(id);

  logger.info('post: ' + JSON.stringify(post));

  if (post.userId != req.userId) {
    throw { status: 403 }
  }

  if (!req.body || !req.body.content) {
    throw { status: 400, message: 'Empty post content' };
  }

  post.content = req.body.content;
  await postsRepository.update(id, req.userId, post);
  return res.send(post);
}

Posts.prototype.add = async function(req, res, next) {
  let id = generateId();
  logger.info(id);
  logger.info(`body: ${JSON.stringify(req.body)}`);

  if (!req.body || !req.body.content) {
    throw { status: 400, message: 'Empty post content' };
  }

  let post = {
    id: id,
    content: req.body.content,
    userId: req.userId,
    created: + new Date(),
    up: 0,
    down: 0
  };

  await postsRepository.add(post);
  return res.send(post);
}

Posts.prototype.get = async function(req, res, next) {
  let id = req.params.id;
  let post = await postsRepository.find(id);
  return res.send(post);
}

const generateId = function() {
  // http://stackoverflow.com/questions/8855687/secure-random-token-in-node-js/36008760#36008760
  const crypto = require('crypto');

  return crypto.randomBytes(7).toString('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=/g, '');
};

module.exports = Posts;
