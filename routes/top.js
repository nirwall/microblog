const logger = require('../logger.js');

let topCollection;
let nowFn;
let lastUpdate;
let cachedEntries;

function Top(coll, now) {
  topCollection = coll;

  nowFn = now;
  let offset = (24*60*60*1000) * 7;
  let f = nowFn();
  f.setTime(f.getTime() - offset);
  lastUpdate = + f;
}

Top.prototype.get = async function(req, res, next) {
  let now = nowFn();
  if (now - lastUpdate > 29 * 1000) {
      let res = await topCollection.findOne({id: 'theList'});
      this.cachedEntries = JSON.parse(res.content);
      lastUpdate = now;
  }
  return res.send(this.cachedEntries);
}

module.exports = Top;