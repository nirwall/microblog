let postsRepository;
let votesRepository;

const logger = require('../logger.js');

function Votes(posts, votes) {
  postsRepository = posts;
  votesRepository = votes;
}

Votes.prototype.upvote = async function(req, res, next) {
  let postId = req.params.postId;
  let userId = req.userId;
  let originalValue = await updateVote(postId, userId, 1);

  await postsRepository.updateUpvotes(postId, 1);
  if (originalValue == -1) {
    await postsRepository.updateDownvotes(postId, -1);
  }

  return res.send();
}

Votes.prototype.downvote = async function(req, res, next) {
  let postId = req.params.postId;
  let userId = req.userId;
  let originalValue = await updateVote(postId, userId, -1);

  await postsRepository.updateDownvotes(postId, 1);
  if (originalValue == 1) {
    await postsRepository.updateUpvotes(postId, -1);
  }
  return res.send();
}

Votes.prototype.unvote = async function(req, res, next) {
  let postId = req.params.postId;
  let userId = req.userId;

  let orig = await votesRepository.delete(postId, userId);

  if (!orig) {
    return res.send();
  } else if(orig.val == 1) {
    await postsRepository.updateUpvotes(postId, -1);
  } else {
    await postsRepository.updateDownvotes(postId, -1);
  }

  return res.send();
}

// change the users vote value in the votes collection
// return the original vote value so the posts collection can be updated
async function updateVote(postId, userId, activeVoteValue) {
  let originalValue = 0;
  let originalVote;

  let post = await postsRepository.find(postId);
  originalVote = await votesRepository.find(postId, userId);

  if (originalVote && originalVote.val == activeVoteValue) {
    throw {
      "status": 400,
      "message": "Already voted"
    };
  }
  originalValue = originalVote ? originalVote.val : 0;

  if (originalVote) {
    let res = await votesRepository
      .update(postId, userId, activeVoteValue);
  } else {
    let newVote = {
      userId: userId,
      postId: postId,
      postCreated: post.created,
      val: activeVoteValue
    }

    await votesRepository.add(newVote);
  }

  return originalValue;
}

module.exports = Votes;
