let postsCollection;

const logger = require('./logger.js');

function PostsRepository(coll) {
  postsCollection = coll;
}

PostsRepository.prototype.find = async function(id) {
  let post = await postsCollection.findOne({ id: id });

  if (post == null) {
    let err = { status: 404, message: `Post doesn't exist` }
    throw err;
  }

  return post;
}

PostsRepository.prototype.recent = async function(since) {
  let posts = await postsCollection.find({ created: { $gt: since } });
  return posts;
}

PostsRepository.prototype.update = async function(id, userId, post) {
  let query = { id: id, userId: userId };
  let updatedPost = await postsCollection
    .findOneAndUpdate(query, post);

  if (updatedPost == null) {
    let err = { status: 404, message: `Post doesn't exist` }
    throw err;
  }
}

PostsRepository.prototype.updateUpvotes = async function(id, count) {
  let query = { id: id };
  let updatedPost = await postsCollection
    .findOneAndUpdate(query, { $inc: { up: count } });

  if (updatedPost == null) {
    let err = { status: 404, message: `Post doesn't exist` }
    throw err;
  }
}

PostsRepository.prototype.updateDownvotes = async function(id, count) {
  let query = { id: id };
  let updatedPost = await postsCollection
    .findOneAndUpdate(query, { $inc: { down: count } });

  if (updatedPost == null) {
    let err = { status: 404, message: `Post doesn't exist` }
    throw err;
  }
}


PostsRepository.prototype.add = async function(post) {
  let res = await postsCollection.insert(post);

  if (res == null) {
    let err = { status: 500 }
    throw err;
  }

  return post;
}


module.exports = PostsRepository;
