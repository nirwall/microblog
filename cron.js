// use this if with a cron statement
// */1 * * * * "node {project-path}/cron.js"

const monk = require('monk');
const db = monk('localhost:27017/test');
db.options = {
  safe: true,
  castIds: false
};

const topCollection = db.get('top');
const postsCollection = db.get('posts');
const postsRepo = new (require('./postsRepository.js'))(postsCollection);
const nowFn = () => new Date();
const topCalculator = new (require('./topCalculator.js'))(postsRepo, nowFn);
(async () => {
  let entries = await topCalculator.get();
  let topList = {
    id: 'theList',
    content: JSON.stringify(entries)
  };
  await topCollection.findOneAndUpdate({ id: 'theList' }, topList, {
    upsert: true
  });
  process.exit();
})();
