const logger = require('./logger.js');

let postsRepository;
let nowFn;

function TopCalculator(repo, now) {
  postsRepository = repo;
  nowFn = now
}

TopCalculator.prototype.get = async function() {

  let offset = (24*60*60*1000) * 7;
  let since = nowFn();
  let nowTs = +since;
  since.setTime(since.getTime() - offset);
  let sinceTs = + since;

  let entries = [];
  let posts = await postsRepository.recent(sinceTs);
  for (let post of posts) {
    // based on https://medium.com/hacking-and-gonzo/how-reddit-ranking-algorithms-work-ef111e33d0d9
    let score = (post.up - post.down) || 0;
    let order = Math.log(Math.max(Math.abs(score), 1)) / Math.log(1.01);
    let sign = Math.sign(score);
    let minutesDiff = (nowTs - post.created) / (1000 * 60);
    let fresh = 70 / Math.pow(Math.log(Math.max(minutesDiff, 1.1)), 0.6);
    let rating = Math.round(sign * order + fresh);

    let entry = { id: post.id, rating: rating }

    // keep to top 10 entries
    entries.push(entry);
    entries.sort(cmp);
    entries = entries.slice(0, 10);
  }

  return entries;
}

const cmp = (a, b) => {
  if (a.rating === b.rating) return 0
  return a.rating < b.rating ? 1 : -1
}

module.exports = TopCalculator;